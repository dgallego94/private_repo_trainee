package com.valid.aliados.balance.consumer.consumerbalance;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import java.time.LocalDateTime;

import com.valid.aliados.balance.consumer.consumerbalance.controllers.ClientRestController;
import com.valid.aliados.balance.consumer.consumerbalance.domain.Client;
import com.valid.aliados.balance.consumer.consumerbalance.domain.DocumentClient;
import com.valid.aliados.balance.consumer.consumerbalance.domain.PhoneNumber;
import com.valid.aliados.balance.consumer.consumerbalance.services.IClientService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.MimeTypeUtils;

/**
 * ClientRestController
 */
@RunWith(SpringRunner.class)
@WebMvcTest(ClientRestController.class)
public class ClientRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    @Qualifier("callBackClientContingency")
    private Client clienteMock;

    @MockBean
    private IClientService clienteServiceTest;

    @Test
    public void returnClientJsonTest() throws Exception {
        when(clienteServiceTest.getClient(any())).thenReturn(factoryClientTest());
        when(clienteMock.getName()).thenReturn("not_found");
        mockMvc.perform(get("/client/12345").accept(MimeTypeUtils.APPLICATION_JSON_VALUE));
    }

    private Client factoryClientTest() {
        PhoneNumber phoneTest = new PhoneNumber(1L, 1L);
        DocumentClient docClientTestt = new DocumentClient("auxiliar_city_not_found", LocalDateTime.now(), 0L,
                "auxiliar_type");
        return new Client(docClientTestt, "notFound@contingency.com", "not_found", phoneTest, 0L);
    }

}