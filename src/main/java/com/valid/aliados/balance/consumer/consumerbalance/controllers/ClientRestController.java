package com.valid.aliados.balance.consumer.consumerbalance.controllers;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.valid.aliados.balance.consumer.consumerbalance.domain.Client;
import com.valid.aliados.balance.consumer.consumerbalance.services.IClientService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * ClientRestController
 */
@RestController
@RequestMapping("/")
public class ClientRestController {

    @Autowired
    private IClientService clientService;

    @Autowired
    @Qualifier("callBackClientContingency")
    private Client clientContingency;

    @GetMapping("/client/{document}")
    @HystrixCommand(fallbackMethod = "contingencyMethod")
    @ResponseStatus(code = HttpStatus.OK)
    public Client returnClientJson(@PathVariable(value = "document") Long document){
        return clientService.getClient(document);
    }
    
    public Client contingencyMethod(){
        return clientContingency;
    }
}