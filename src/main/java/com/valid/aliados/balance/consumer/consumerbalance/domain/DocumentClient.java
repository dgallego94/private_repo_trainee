package com.valid.aliados.balance.consumer.consumerbalance.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * DocumentClient
 */
public class DocumentClient implements Serializable {

    /**
     * Fields Attributes
     */
    private String expeditionCity;
    private LocalDateTime expeditionDate;
    private Long documentNumber;
    private String type;
    /**
     * Default generated ID by Serializable
     */
    private static final long serialVersionUID = 9220151171075786241L;

    /**
     * Constructors
     */

    public DocumentClient() {
    }

    public DocumentClient(String expeditionCity, LocalDateTime expeditionDate, Long documentNumber, String type) {
        this.expeditionCity = expeditionCity;
        this.expeditionDate = expeditionDate;
        this.documentNumber = documentNumber;
        this.type = type;
    }

    /**
     * GET and SET
     */

    public String getExpeditionCity() {
        return expeditionCity;
    }

    public void setExpeditionCity(String expeditionCity) {
        this.expeditionCity = expeditionCity;
    }

    public LocalDateTime getExpeditionDate() {
        return expeditionDate;
    }

    public void setExpeditionDate(LocalDateTime expeditionDate) {
        this.expeditionDate = expeditionDate;
    }

    public Long getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(Long documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}