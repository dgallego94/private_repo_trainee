package com.valid.aliados.balance.consumer.consumerbalance;

import java.time.LocalDateTime;

import com.valid.aliados.balance.consumer.consumerbalance.domain.Client;
import com.valid.aliados.balance.consumer.consumerbalance.domain.DocumentClient;
import com.valid.aliados.balance.consumer.consumerbalance.domain.PhoneNumber;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigApp {

    private static LocalDateTime localDateTime = LocalDateTime.now();

    @Bean(name = "callBackClientContingency")
    public Client callingContingencyClient() {
        return new Client(auxiliarDocClient(), "notFound@contingency.com", "not_found", phoneNumberHelper(), 0L);

    }

    private DocumentClient auxiliarDocClient() {
        return new DocumentClient("auxiliar_city_not_found", localDateTime, 0L, "auxiliar_type");
    }

    private PhoneNumber phoneNumberHelper() {
        return new PhoneNumber(0L, 0L);
    }
}