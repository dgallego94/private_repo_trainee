package com.valid.aliados.balance.consumer.consumerbalance.clients;

import com.valid.aliados.balance.consumer.consumerbalance.domain.Client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "missing-service") // pendiente micro
public interface ConsumerFeign {

    @GetMapping(path = "/dniclient/{id}") // pendiente por solicitud
    Client getClientJson(@PathVariable(value = "id") Long document);

}