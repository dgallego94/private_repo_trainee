package com.valid.aliados.balance.consumer.consumerbalance.domain;

import java.io.Serializable;

/**
 * Client
 */
public class Client implements Serializable {

    /**
     * Fields Attributes
     */

    private DocumentClient documentClient;
    private String email;
    private String name;
    private PhoneNumber phoneNumber;
    private Long accountNumber;

    /**
     * Default Generated ID b serializable
     */
    private static final long serialVersionUID = 2114342730060903380L;

    /**
     * Constructors
     */
    public Client() {
    }

    public Client(DocumentClient documentClient, String email, String name, PhoneNumber phoneNumber,
            Long accountNumber) {
        this.documentClient = documentClient;
        this.email = email;
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.accountNumber = accountNumber;
    }

    public DocumentClient getDocumentClient() {
        return documentClient;
    }

    public void setDocumentClient(DocumentClient documentClient) {
        this.documentClient = documentClient;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PhoneNumber getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(PhoneNumber phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(Long accountNumber) {
        this.accountNumber = accountNumber;
    }

    /**
     * 
     * GET and SET
     */

}