package com.valid.aliados.balance.consumer.consumerbalance.services;

import com.valid.aliados.balance.consumer.consumerbalance.domain.Client;

/**
 * IClientService
 */
public interface IClientService {

    Client getClient(Long documentId);

}