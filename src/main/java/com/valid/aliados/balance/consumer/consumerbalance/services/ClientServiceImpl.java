package com.valid.aliados.balance.consumer.consumerbalance.services;

import com.valid.aliados.balance.consumer.consumerbalance.clients.ConsumerFeign;
import com.valid.aliados.balance.consumer.consumerbalance.domain.Client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * ClientServiceImpl
 */
@Service(value = "clientService")
public class ClientServiceImpl implements IClientService {

    @Autowired
    private ConsumerFeign consumerFeign;

    @Override
    public Client getClient(Long documentId) {

        return consumerFeign.getClientJson(documentId);
    }

}