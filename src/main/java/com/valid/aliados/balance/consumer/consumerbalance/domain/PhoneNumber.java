package com.valid.aliados.balance.consumer.consumerbalance.domain;

import java.io.Serializable;

/**
 * PhoneNumber
 */
public class PhoneNumber implements Serializable {

    /**
     * Fields Attributes
     */
    private Long countryId;
    private Long phoneNumber;
    /**
     * Default generated Id by Serializable
     */
    private static final long serialVersionUID = -7518448313847706048L;

    /**
     * Constructors
     */
    public PhoneNumber() {
    }

    public PhoneNumber(Long countryId, Long phoneNumber) {
        this.countryId = countryId;
        this.phoneNumber = phoneNumber;
    }

    /**
     * GET and SET
     */
    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    public Long getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Long phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

}